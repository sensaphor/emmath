cmake_minimum_required(VERSION 3.7)
project(emstd-math CXX)

###############################################################################
# General setup

get_filename_component(EMSTD_MATH_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/../" ABSOLUTE)
if (NOT DEFINED EMSTD_MATH_BUILD_DIR)
  set(EMSTD_MATH_BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR})
endif()
set(LIBRARY_OUTPUT_PATH ${EMSTD_MATH_BUILD_DIR}/lib)
set(EXECUTABLE_OUTPUT_PATH ${EMSTD_MATH_BUILD_DIR}/bin)

# C++11 or better
set(EMSTD_MATH_FEATURES cxx_relaxed_constexpr)

set(EMSTD_MATH_WARNING_FLAGS -Wall -Wextra -pedantic -Werror=return-type -Wnon-virtual-dtor PARENT_SCOPE)

###############################################################################
# External packages

add_subdirectory("${EMSTD_MATH_ROOT}/external" "${EMSTD_MATH_BUILD_DIR}/external")

###############################################################################
# emstd-math package

add_library(emstd-math INTERFACE)

set_target_properties(emstd-math PROPERTIES
  INTERFACE_INCLUDE_DIRECTORIES "${EMSTD_MATH_ROOT}/include"
  INTERFACE_COMPILE_FEATURES "${EMSTD_MATH_FEATURES}"
  INTERFACE_LINK_LIBRARIES "Emstd::Core")

install(
  DIRECTORY ${EMSTD_MATH_ROOT}/include
  DESTINATION .
  FILES_MATCHING PATTERN "*.[hi]pp")
