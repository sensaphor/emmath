///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_NUMBERS_HPP
#define EMSTD_MATH_NUMBERS_HPP

#include <emstd/detail/config.hpp>

namespace emstd
{
namespace math
{
namespace numbers
{

//-----------------------------------------------------------------------------
// P0631

template <typename T>
EMSTD_ENABLE_CXX_17(inline) constexpr T e_v{T(2.718281828459045235360287471352662497757247093699959574966967627724076630353547594)};
EMSTD_ENABLE_CXX_17(inline) constexpr double e = e_v<double>;

template <typename T>
EMSTD_ENABLE_CXX_17(inline) constexpr T pi_v{T(3.141592653589793238462643383279502884197169399375105820974944592307816406286208998)};
EMSTD_ENABLE_CXX_17(inline) constexpr double pi = pi_v<double>;

template <typename T>
EMSTD_ENABLE_CXX_17(inline) constexpr T sqrt2_v{T(1.414213562373095048801688724209698078569671875376948073176679737990732478462107038)};
EMSTD_ENABLE_CXX_17(inline) constexpr double sqrt2 = sqrt2_v<double>;

//-----------------------------------------------------------------------------
// Extension

template <typename T>
constexpr T reciprocal(T x) { return T(1) / x; }

// e^(1/2)
template <typename T>
EMSTD_ENABLE_CXX_17(inline) constexpr T sqrt_e_v{T(1.648721270700128146848650787814163571653776100710148011575079311640661021194215608)};
EMSTD_ENABLE_CXX_17(inline) constexpr double sqrt_e = sqrt_e_v<double>;

} // namespace numbers
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_NUMBERS_HPP
