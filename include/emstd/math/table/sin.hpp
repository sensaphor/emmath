///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_TABLE_SIN_HPP
#define EMSTD_MATH_TABLE_SIN_HPP

#include <emstd/math/table/bessel.hpp>

namespace emstd
{
namespace math
{
namespace table
{

template <typename T, std::size_t N>
struct sin : array<T, N>
{
    constexpr sin(T alpha)
    {
        // Chebyshev spectral coefficients
        // { 0, J_1, 0, -J_3, ..., (-1)^(N+1) J_N }
        bessel<T, N> co(alpha);
        for (decltype(N) k = 0; k < N; ++k)
        {
            switch (k & 3)
            {
            case 1:
                (*this)[k] = co[k];
                break;
            case 3:
                (*this)[k] = -co[k];
                break;
            default:
                (*this)[k] = T(0);
                break;
            }
        }
    }
};

} // namespace table
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_TABLE_SIN_HPP
