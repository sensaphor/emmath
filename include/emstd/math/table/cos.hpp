///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_TABLE_COS_HPP
#define EMSTD_MATH_TABLE_COS_HPP

#include <emstd/math/table/bessel.hpp>

namespace emstd
{
namespace math
{
namespace table
{

template <typename T, std::size_t N>
struct cos : array<T, N>
{
    constexpr cos(T alpha)
    {
        // Chebyshev spectral coefficients
        // { J_0, 0, J_2, 0, -J_4, 0, ..., (-1)^N J_N }
        bessel<T, N> co(alpha);
        (*this)[0] = co[0];
        for (decltype(N) k = 1; k < N; ++k)
        {
            switch (k & 3)
            {
            case 0:
                (*this)[k] = co[k];
                break;
            case 2:
                (*this)[k] = -co[k];
                break;
            default:
                (*this)[k] = T(0);
                break;
            }
        }
    }
};

} // namespace table
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_TABLE_COS_HPP
