///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_TABLE_EXP_HPP
#define EMSTD_MATH_TABLE_EXP_HPP

#include <emstd/math/table/bessel.hpp>

namespace emstd
{
namespace math
{
namespace table
{

template <typename T, std::size_t N>
struct exp
    : modified_bessel<T, N>
{
    constexpr exp(T alpha)
        : modified_bessel<T, N>(alpha)
    {
    }
};

} // namespace table
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_TABLE_EXP_HPP
