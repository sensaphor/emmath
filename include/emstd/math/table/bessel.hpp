///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_TABLE_BESSEL_HPP
#define EMSTD_MATH_TABLE_BESSEL_HPP

#include <limits>
#include <emstd/array.hpp>

namespace emstd
{
namespace math
{

template <typename T>
constexpr T bessel_recurrence(T x, T k, T current, T old)
{
    return current * T(2) * k / x - old;
}

template <typename T>
constexpr T modified_bessel_recurrence(T x, T k, T current, T old)
{
    return current * T(2) * k / x + old;
}

namespace table
{

//-----------------------------------------------------------------------------

template <typename T, std::size_t N>
struct bessel
    : array<T, N>
{
    constexpr bessel(T x)
    {
        // Bessel J_k(x) can be calculated directly with the above recurrence
        // in a forward pass, but this is numerically unstable because the J_k
        // sequence is exponentially decreasing.
        //
        // A numerically stable algorithm is to calculate a backwards pass as
        // described in:
        //
        //   Miller, "On the choice of standard solutions for a homogeneous
        //   linear equation of the second order", Quarterly Journal of
        //   Mechanics and Applied Mathematics, 3(2), pp. 225-235, 1950.

        T jkkk = T(0);
        T jkk = T(1);
        T norm = T(0);
        for (decltype(N) k = N - 1; k < N; --k) // k = N-1, ..., 0
        {
            auto jk = bessel_recurrence(x, T(k + 1), jkk, jkkk);
            // [NIST:10.12.4] 1 = J_0 + 2 J_2 + 2 J_4 + ...
            norm += jk * ((k & 1) == 0) * (T(1) + (k != 0));
            (*this)[k] = jk;
            jkkk = jkk;
            jkk = jk;
        }
        // Normalization
        for (decltype(N) k = 0; k < N; ++k)
        {
            (*this)[k] /= norm;
        }
    }
};

//-----------------------------------------------------------------------------

template <typename T, std::size_t N>
struct modified_bessel
    : array<T, N>
{
    constexpr modified_bessel(T x)
    {
         // Bessel I_k(x)

        T ikkk = T(0);
        T ikk = T(1);
        T norm = T(0);
        for (decltype(N) k = N - 1; k < N; --k) // k = N-1, ..., 0
        {
            auto ik = modified_bessel_recurrence(x, T(k + 1), ikk, ikkk);
            // [NIST:10.35.4] 1 = I_0 - 2 I_2 + 2 I_4 - ...
            norm += ik * ((k & 1) == 0) * ((k & 3) == 0 ? T(1) : T(-1)) * (T(1) + (k != 0));
            (*this)[k] = ik;
            ikkk = ikk;
            ikk = ik;
        }
        // Normalization
        for (decltype(N) k = 0; k < N; ++k)
        {
            (*this)[k] /= norm;
        }
    }
};

} // namespace table
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_TABLE_BESSEL_HPP
