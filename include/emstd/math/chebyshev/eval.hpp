///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_CHEBYSHEV_EVAL_HPP
#define EMSTD_MATH_CHEBYSHEV_EVAL_HPP

namespace emstd
{
namespace math
{
namespace chebyshev
{

//-----------------------------------------------------------------------------
// Recurrence relation

template <typename T>
constexpr T recurrence(T x, T current, T old)
{
    return T(2) * x * current - old;
}

//-----------------------------------------------------------------------------
// Clenshaw, "A note on the summation of Chebyshev series", Mathematics of
// Computation, 9(51), pp. 118-120, 1955.
//
// Mandates:
// - T is a floating-point type.

template <unsigned N, typename T>
constexpr T backward(T x, const T* co)
{
    auto yk2 = T(0);
    auto yk1 = T(0);
    for (unsigned k = N - 1; k > 0; --k)
    {
        auto yk = recurrence(x, yk1, yk2) + co[k];
        yk2 = yk1;
        yk1 = yk;
    }
    return co[0] + x * yk1 - yk2;
}

template <typename T, unsigned N>
constexpr T backward(T x, const T(&co)[N])
{
    return backward<N>(x, static_cast<const T*>(co));
}

template <template <typename, std::size_t> class A, typename T, std::size_t N>
constexpr T backward(T x, const A<T, N>& co)
{
    return backward<N>(x, co.data());
}

} // namespace chebyshev
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_CHEBYSHEV_EVAL_HPP
