///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_CHEBYSHEV_SIMILARITY_HPP
#define EMSTD_MATH_CHEBYSHEV_SIMILARITY_HPP

namespace emstd
{
namespace math
{
namespace chebyshev
{

//-----------------------------------------------------------------------------
// Returns cosine similarity between two normalized polynomials.

template <std::size_t N, typename T>
constexpr T similarity(const T* lhs, const T* rhs)
{
    // Similar to the inner product, but scaled so results are between -1 and 1.
    //
    // Trefetchen, "Approximation Theory and Approximation Practice", 2013,
    // p. 124:
    //
    //  "if T_0 is replaced by T_0 / sqrt(2), [the Chebyshev polynomials] are
    //   orthonormal"
    //
    // 2 / (sqrt(2) * sqrt(2)) = 1

    T result = lhs[0] * rhs[0];
    for (std::size_t k = 1; k < N; ++k)
    {
        result += lhs[k] * rhs[k];
    }
    return result;
}

// Convenience

template <template <typename, std::size_t> class A, typename T, std::size_t N>
constexpr T similarity(const A<T, N>& lhs, const A<T, N>& rhs)
{
    return similarity<N>(lhs.data(), rhs.data());
}

template <typename T, std::size_t N>
constexpr T similarity(const T(&lhs)[N], const T(&rhs)[N])
{
    return similarity<N>(lhs, rhs);
}

} // namespace chebyshev
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_CHEBYSHEV_SIMILARITY_HPP
