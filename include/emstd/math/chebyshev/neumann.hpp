///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_CHEBYSHEV_NEUMANN_HPP
#define EMSTD_MATH_CHEBYSHEV_NEUMANN_HPP

#include <emstd/array.hpp>

namespace emstd
{
namespace math
{
namespace chebyshev
{

// Neumann factor

template <typename Array>
constexpr auto neumann(const Array& array)
    -> emstd::array<typename_value_t<Array>, core::array_size<Array>::value>
{
    emstd::array<typename_value_t<Array>, core::array_size<Array>::value> result{};
    result[0] = array[0];
    for (typename_size_t<Array> k = 1; k < array.size(); ++k)
    {
        result[k] = 2 * array[k];
    }
    return result;
}

} // namespace chebyshev
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_CHEBYSHEV_NEUMANN_HPP
