///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_CHEBYSHEV_PARITY_HPP
#define EMSTD_MATH_CHEBYSHEV_PARITY_HPP

#include <emstd/array.hpp>

namespace emstd
{
namespace math
{
namespace chebyshev
{

//-----------------------------------------------------------------------------
// Even-parity coefficients

template <typename Array>
constexpr auto even(const Array& array)
    -> emstd::array<typename_value_t<Array>, (core::array_size<Array>::value + 1) / 2>
{
    emstd::array<typename_value_t<Array>, (core::array_size<Array>::value + 1) / 2> result{};
    for (typename_size_t<Array> k = 0; k < result.size(); ++k)
    {
        result[k] = array[2 * k];
    }
    return result;
}

//-----------------------------------------------------------------------------
// Odd-parity coefficients
//
// H_{2k} = sum (-1)^m F_{2k+2m+1}
//
// H_{2N+2} = 0
// H_{2k} = 2 F_{2k+1} - H_{2k+2}
// H_0 = 2 F_1 - H_2 / 2

template <typename Array>
constexpr auto odd(const Array& array)
    -> emstd::array<typename_value_t<Array>, core::array_size<Array>::value / 2>
{
    emstd::array<typename_value_t<Array>, core::array_size<Array>::value / 2> result{};
    auto previous = typename_value_t<Array>(0);
    for (typename_size_t<Array> k = result.size() - 1; k > 0; --k)
    {
        auto current = array[2 * k + 1] - previous;
        result[k] = 2 * current;
        previous = current;
    }
    result[0] = array[1] - previous;
    return result;
}

} // namespace chebyshev
} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_CHEBYSHEV_PARITY_HPP
