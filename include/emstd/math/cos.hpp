///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_COS_HPP
#define EMSTD_MATH_COS_HPP

#include <emstd/core/numeric_traits.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/table/cos.hpp>
#include <emstd/math/chebyshev/neumann.hpp>
#include <emstd/math/chebyshev/parity.hpp>
#include <emstd/math/chebyshev/eval.hpp>

namespace emstd
{
namespace math
{
namespace detail
{

template <typename>
struct cos_trait;

template <>
struct cos_trait<float>
{
    enum : size_t { exact = 10 };
};

#if defined(EMSTD_MATH_COMPLEX)

template <>
struct cos_trait<complex::number<float>>
{
    enum : size_t { exact = 12 };
};

#endif

} // namespace detail

//-----------------------------------------------------------------------------
// cos(pi * x) where x in [-1,1]

EMSTD_INLINE_VARIABLE struct
{
    template <size_t N, typename T>
    constexpr T operator()(index_constant<N>, T x) const
    {
        constexpr auto co(chebyshev::even(chebyshev::neumann(table::cos<T, 2 * N>(numbers::pi_v<T>))));
        T x2 = chebyshev::recurrence(x, x, T(1));
        return chebyshev::backward(x2, array_of(co));
    }

    template <typename T>
    constexpr T operator()(T x) const
    {
        return (*this)(index_constant<detail::cos_trait<numeric_scalar_t<T>>::exact>{}, x);
    }
} cos_pi{};

} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_COS_HPP
