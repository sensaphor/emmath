///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#ifndef EMSTD_MATH_SIN_HPP
#define EMSTD_MATH_SIN_HPP

#include <emstd/core/numeric_traits.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/table/sin.hpp>
#include <emstd/math/chebyshev/neumann.hpp>
#include <emstd/math/chebyshev/parity.hpp>
#include <emstd/math/chebyshev/eval.hpp>

namespace emstd
{
namespace math
{
namespace detail
{

template <typename>
struct sin_trait;

template <>
struct sin_trait<float>
{
    enum : size_t { exact = 10 };
};

} // namespace detail

//-----------------------------------------------------------------------------
// sin(pi * x) where x in [-1,1]

EMSTD_INLINE_VARIABLE struct
{
    template <size_t N, typename T>
    constexpr T operator()(index_constant<N>, T x) const
    {
        constexpr auto co = chebyshev::odd(chebyshev::neumann(table::sin<T, 2 * N>(numbers::pi_v<T>)));
        T x2 = chebyshev::recurrence(x, x, T(1));
        return x * chebyshev::backward(x2, array_of(co));
    }

    template <typename T>
    constexpr T operator()(T x) const
    {
        return (*this)(index_constant<detail::sin_trait<numeric_scalar_t<T>>::exact>{}, x);
    }
} sin_pi{};

} // namespace math
} // namespace emstd

#endif // EMSTD_MATH_SIN_HPP
