///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/array.hpp>
#include <emstd/math/chebyshev/parity.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_even
{

constexpr auto even1 = chebyshev::even(emstd::array<float,1>{0.0f});

static_assert(even1.size() == 1, "");
static_assert(even1[0] == 0.0f, "");

constexpr auto even2 = chebyshev::even(emstd::array<float,2>{0.0f, 1.0f});

static_assert(even2.size() == 1, "");
static_assert(even2[0] == 0.0f, "");

constexpr auto even3 = chebyshev::even(emstd::array<float,3>{0.0f, 1.0f, 2.0f});

static_assert(even3.size() == 2, "");
static_assert(even3[0] == 0.0f, "");
static_assert(even3[1] == 2.0f, "");

constexpr auto even4 = chebyshev::even(emstd::array<float,4>{0.0f, 1.0f, 2.0f, 3.0f});

static_assert(even4.size() == 2, "");
static_assert(even4[0] == 0.0f, "");
static_assert(even4[1] == 2.0f, "");

} // namespace suite_even

//-----------------------------------------------------------------------------

namespace suite_odd
{

constexpr auto odd2 = chebyshev::odd(emstd::array<float,2>{0.0f, 1.0f});

static_assert(odd2.size() == 1, "");
static_assert(odd2[0] == 1.0f, "");

constexpr auto odd3 = chebyshev::odd(emstd::array<float,3>{0.0f, 1.0f, 2.0f});

static_assert(odd3.size() == 1, "");
static_assert(odd3[0] == 1.0f, "");

constexpr auto odd4 = chebyshev::odd(emstd::array<float,4>{0.0f, 1.0f, 2.0f, 3.0f});

static_assert(odd4.size() == 2, "");
static_assert(odd4[0] == -2.0f, "");
static_assert(odd4[1] == 6.0f, "");

constexpr auto odd5 = chebyshev::odd(emstd::array<float,5>{0.0f, 1.0f, 2.0f, 3.0f, 4.0f});

static_assert(odd5.size() == 2, "");
static_assert(odd5[0] == -2.0f, "");
static_assert(odd5[1] == 6.0f, "");

} // namespace suite_odd

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
