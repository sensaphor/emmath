///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2022 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/chebyshev/similarity.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_similarity_float
{

constexpr emstd::close_to<float> equals;

// Basis

constexpr float t0[]{ 1.0f, 0.0f, 0.0f, 0.0f };
constexpr float t1[]{ 0.0f, 1.0f, 0.0f, 0.0f };
constexpr float t2[]{ 0.0f, 0.0f, 1.0f, 0.0f };
constexpr float t3[]{ 0.0f, 0.0f, 0.0f, 1.0f };

// Monomials

constexpr float x2[]{ 0.5f, 0.0f, 0.5f, 0.0f };
constexpr float x3[]{ 0.0f, 0.75f, 0.0f, 0.25f };

static_assert(equals(chebyshev::similarity(t0, t0), 1.0), "");
static_assert(equals(chebyshev::similarity(t1, t0), 0.0), "");
static_assert(equals(chebyshev::similarity(t1, t1), 1.0), "");
static_assert(equals(chebyshev::similarity(t2, t0), 0.0), "");
static_assert(equals(chebyshev::similarity(t2, t1), 0.0), "");
static_assert(equals(chebyshev::similarity(t2, t2), 1.0), "");
static_assert(equals(chebyshev::similarity(t3, t0), 0.0), "");
static_assert(equals(chebyshev::similarity(t3, t1), 0.0), "");
static_assert(equals(chebyshev::similarity(t3, t2), 0.0), "");
static_assert(equals(chebyshev::similarity(t3, t3), 1.0), "");

static_assert(equals(chebyshev::similarity(x2, t0), 0.5), "");
static_assert(equals(chebyshev::similarity(x2, t1), 0.0), "");
static_assert(equals(chebyshev::similarity(x2, t2), 0.5), "");
static_assert(equals(chebyshev::similarity(x2, t3), 0.0), "");
static_assert(equals(chebyshev::similarity(x2, x2), 0.5), "");

static_assert(equals(chebyshev::similarity(x3, x2), 0.0), "");
static_assert(equals(chebyshev::similarity(x3, x3), 0.625), "");
static_assert(equals(chebyshev::similarity(x3, t0), 0.0), "");
static_assert(equals(chebyshev::similarity(x3, t1), 0.75), "");
static_assert(equals(chebyshev::similarity(x3, t2), 0.0), "");
static_assert(equals(chebyshev::similarity(x3, t3), 0.25), "");

} // namespace suite_similarity_float

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
