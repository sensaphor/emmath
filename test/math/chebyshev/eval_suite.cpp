///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/array.hpp>
#include <emstd/math/chebyshev/eval.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_backward
{

constexpr emstd::close_to<float> equals;

 // 2 x^2 + x
constexpr emstd::array<float, 3> array3{1.0f, 1.0f, 1.0f};

static_assert(equals(chebyshev::backward(-1.0f, array_of(array3)), 1.0f), "");
static_assert(equals(chebyshev::backward(-0.75f, array_of(array3)), 0.375f), "");
static_assert(equals(chebyshev::backward(-0.5f, array_of(array3)), 0.0f), "");
static_assert(equals(chebyshev::backward(-0.25f, array_of(array3)), -0.125f), "");
static_assert(equals(chebyshev::backward(0.0f, array_of(array3)), 0.0f), "");
static_assert(equals(chebyshev::backward(0.25f, array_of(array3)), 0.375f), "");
static_assert(equals(chebyshev::backward(0.5f, array_of(array3)), 1.0f), "");
static_assert(equals(chebyshev::backward(0.75f, array_of(array3)), 1.875f), "");
static_assert(equals(chebyshev::backward(1.0f, array_of(array3)), 3.0f), "");

// 4 x^3 + 2 x^2 - 2 x
constexpr emstd::array<float, 4> array4{1.0f, 1.0f, 1.0f, 1.0f};

static_assert(equals(chebyshev::backward(-1.0f, array_of(array4)), 0.0f), "");
static_assert(equals(chebyshev::backward(-0.75f, array_of(array4)), 0.9375f), "");
static_assert(equals(chebyshev::backward(-0.5f, array_of(array4)), 1.0f), "");
static_assert(equals(chebyshev::backward(-0.25f, array_of(array4)), 0.5625f), "");
static_assert(equals(chebyshev::backward(0.0f, array_of(array4)), 0.0f), "");
static_assert(equals(chebyshev::backward(0.25f, array_of(array4)), -0.3125f), "");
static_assert(equals(chebyshev::backward(0.5f, array_of(array4)), 0.0f), "");
static_assert(equals(chebyshev::backward(0.75f, array_of(array4)), 1.3125f), "");
static_assert(equals(chebyshev::backward(1.0f, array_of(array4)), 4.0f), "");

} // namespace suite_backward

//-----------------------------------------------------------------------------

int main()
{
    return 0;
}
