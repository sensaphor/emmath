///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/complex.hpp>
#include <emstd/math/exp.hpp>

using namespace emstd;
using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_exp_float
{

constexpr emstd::close_to<float> equals;

static_assert(equals(exp(-1.0f), numbers::reciprocal(numbers::e_v<float>)), "");
static_assert(equals(exp(-0.5f), numbers::reciprocal(numbers::sqrt_e_v<float>)), "");
static_assert(equals(exp(0.0f), 1.0f), "");
static_assert(equals(exp(0.5f), numbers::sqrt_e_v<float>), "");
static_assert(equals(exp(1.0f), numbers::e_v<float>), "");

void exp_default()
{
    assert(equals(exp(-1.0f), numbers::reciprocal(numbers::e_v<float>)));
    assert(equals(exp(-0.75f), 0.47236655f));
    assert(equals(exp(-0.5f), numbers::reciprocal(numbers::sqrt_e_v<float>)));
    assert(equals(exp(-0.25f), 0.77880078f));
    assert(equals(exp(0.0f), 1.0f));
    assert(equals(exp(0.25f), 1.2840254f));
    assert(equals(exp(0.5f), numbers::sqrt_e_v<float>));
    assert(equals(exp(0.75f), 2.117f));
    assert(equals(exp(1.0f), numbers::e_v<float>));
}

void exp_approx()
{
    constexpr emstd::close_to<float> approximates(1e-2f);

    assert(approximates(exp(index_constant<4>{}, -1.0f), numbers::reciprocal(numbers::e_v<float>)));
    assert(approximates(exp(index_constant<4>{}, 0.0f), 1.0f));
    assert(approximates(exp(index_constant<4>{}, 1.0f), numbers::e_v<float>));

    constexpr auto exp_approx = bind_front(exp, index_constant<4>{});
    assert(approximates(exp_approx(-1.0f), numbers::reciprocal(numbers::e_v<float>)));
    assert(approximates(exp_approx(0.0f), 1.0f));
    assert(approximates(exp_approx(1.0f), numbers::e_v<float>));
}

void run()
{
    exp_default();
    exp_approx();
}

} // namespace suite_exp_float

//-----------------------------------------------------------------------------

namespace suite_exp_complex_float
{

constexpr emstd::close_to<float> equals;

static_assert(equals(real(exp(complex::number<float>{0.0f})), 1.0f), "");
static_assert(equals(imag(exp(complex::number<float>{0.0f})), 0.0f), "");

void exp_default()
{
    {
        complex::number<float> number{-1.0f};
        auto result = exp(number);
        assert(equals(real(result), numbers::reciprocal(numbers::e_v<float>)));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{-0.5f};
        auto result = exp(number);
        assert(equals(real(result), numbers::reciprocal(numbers::sqrt_e_v<float>)));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{0.0f};
        auto result = exp(number);
        assert(equals(real(result), 1.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{0.5f};
        auto result = exp(number);
        assert(equals(real(result), numbers::sqrt_e_v<float>));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{1.0f};
        auto result = exp(number);
        assert(equals(real(result), numbers::e_v<float>));
        assert(equals(imag(result), 0.0f));
    }
}

void exp_rotate()
{
    {
        complex::number<float> number{-1.0f, 0.0f};
        auto result = exp(number);
        assert(equals(real(result), numbers::reciprocal(numbers::e_v<float>)));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{-0.5f, -0.5f};
        auto result = exp(number);
        assert(equals(real(result), 0.53228073f));
        assert(equals(imag(result), -0.29078629f));
    }
    {
        complex::number<float> number{0.0f, 1.0f};
        auto result = exp(number);
        assert(equals(real(result), 0.54030231f));
        assert(equals(imag(result), 0.84147098f));
    }
    {
        complex::number<float> number{0.5f, 0.5f};
        auto result = exp(number);
        assert(equals(real(result), 1.44688904f));
        assert(equals(imag(result), 0.79043908f));
    }
    {
        complex::number<float> number{1.0f, 0.0f};
        auto result = exp(number);
        assert(equals(real(result), numbers::e_v<float>));
        assert(equals(imag(result), 0.0f));
    }
}

void run()
{
    exp_default();
    exp_rotate();
}

} // namespace suite_exp_complex_float

//-----------------------------------------------------------------------------

int main()
{
    suite_exp_float::run();
    suite_exp_complex_float::run();
    return 0;
}
