///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/complex.hpp>
#include <emstd/math/cos.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_cos_pi_float
{

constexpr emstd::close_to<float> equals;

static_assert(equals(cos_pi(-1.0f), -1.0f), "");
static_assert(equals(cos_pi(0.0f), 1.0f), "");
static_assert(equals(cos_pi(1.0f), -1.0f), "");

void cos_default()
{
    assert(equals(cos_pi(-1.0f), -1.0f));
    assert(equals(cos_pi(-0.75f), -numbers::sqrt2_v<float> / 2));
    assert(equals(cos_pi(-0.5f), 0.0f));
    assert(equals(cos_pi(-0.25f), numbers::sqrt2_v<float> / 2));
    assert(equals(cos_pi(0.0f), 1.0f));
    assert(equals(cos_pi(0.25f), numbers::sqrt2_v<float> / 2));
    assert(equals(cos_pi(0.5f), 0.0f));
    assert(equals(cos_pi(0.75f), -numbers::sqrt2_v<float> / 2));
    assert(equals(cos_pi(1.0f), -1.0f));
}

void run()
{
    cos_default();
}

} // namespace suite_cos_pi_float

//-----------------------------------------------------------------------------

namespace suite_cos_pi_complex_float
{

void cos_default()
{
    constexpr emstd::close_to<float> equals;

    {
        complex::number<float> number{-1.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), -1.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{-0.5f};
        auto result = cos_pi(number);
        assert(equals(real(result), 0.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{0.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), 1.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{0.5f};
        auto result = cos_pi(number);
        assert(equals(real(result), 0.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{1.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), -1.0f));
        assert(equals(imag(result), 0.0f));
    }
}

void cos_rotate()
{
    constexpr emstd::close_to<float> equals(1e-6);

    {
        auto number = complex::number<float>{-1.0f, 0.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), -1.0f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{-0.5f, -0.5f};
        auto result = cos_pi(number);
        assert(equals(real(result), 0.0f));
        assert(equals(imag(result), -2.3012989f));
    }
    {
        complex::number<float> number{0.0f, 1.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), 11.591953f));
        assert(equals(imag(result), 0.0f));
    }
    {
        complex::number<float> number{0.5f, 0.5f};
        auto result = cos_pi(number);
        assert(equals(real(result), 0.0f));
        assert(equals(imag(result), -2.3012989f));
    }
    {
        complex::number<float> number{1.0f, 0.0f};
        auto result = cos_pi(number);
        assert(equals(real(result), -1.0f));
        assert(equals(imag(result), 0.0f));
    }
}

void run()
{
    cos_default();
    cos_rotate();
}

} // namespace suite_cos_pi_complex_float

//-----------------------------------------------------------------------------

int main()
{
    suite_cos_pi_float::run();
    suite_cos_pi_complex_float::run();
    return 0;
}
