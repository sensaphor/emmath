///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/sin.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace suite_sin_pi_float
{

constexpr emstd::close_to<float> equals;

static_assert(equals(sin_pi(-1.0f), 0.0f), "");
static_assert(equals(sin_pi(0.0f), 0.0f), "");
static_assert(equals(sin_pi(1.0f), 0.0f), "");

void test_default()
{
    assert(equals(sin_pi(-1.0f), 0.0f));
    assert(equals(sin_pi(-0.75f), -numbers::sqrt2_v<float> / 2));
    assert(equals(sin_pi(-0.5f), -1.0f));
    assert(equals(sin_pi(-0.25f), -numbers::sqrt2_v<float> / 2));
    assert(equals(sin_pi(0.0f), 0.0f));
    assert(equals(sin_pi(0.25f), numbers::sqrt2_v<float> / 2));
    assert(equals(sin_pi(0.5f), 1.0f));
    assert(equals(sin_pi(0.75f), numbers::sqrt2_v<float> / 2));
    assert(equals(sin_pi(1.0f), 0.0f));
}

void run()
{
    test_default();
}

} // namespace suite_sin_pi_float

//-----------------------------------------------------------------------------

int main()
{
    suite_sin_pi_float::run();
    return 0;
}
