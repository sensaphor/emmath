///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/table/sin.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace test
{

template <typename T>
struct sin : emstd::array<T, 24>
{
private:
    using base = emstd::array<T, 24>;

public:
    // (-1)^n 2 BesselJ[2n, pi] calculated by Mathmatica

    constexpr sin()
        : base{ T(0),
                T(0.284615343179752757345310599686131405709811181849465727564553646963867567128054101),
                T(0),
                T(-0.333458336202989535390218581739886748302244004453913218354781570550951632685630731),
                T(0),
                T(0.052141184367118474740460126093102196955821723437313133385905977554891783829069346),
                T(0),
                T(-0.003420316768495789504925687251642531955341854051173950480063776195555323271515558),
                T(0),
                T(0.000125003442475193113826107929503774086502320588105831554487023901548901375349931),
                T(0),
                T(-2.925124154319571845858558096985975327551927019833358078393480829568822466058e-6),
                T(0),
                T(4.767386375149700570022033875150329231323257371531317478584120797118805899235e-8),
                T(0),
                T(-5.72819220854731575673782309073342965171837651102264567532195194888490498544e-10),
                T(0),
                T(5.28713630876956429434949108236398717542587535585005755161223691797604463326e-12),
                T(0),
                T(-3.86763549770215354707832314313443121886122534846503326777162764261834309984e-14),
                T(0),
                T(2.29797807309147972959784582171510211929283552995237422843075712327175499807e-16),
                T(0),
                T(-1.13115296409870555215633021809121315400367567817788888601218977647471285711e-18) }
    {
    }
};

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_sin_pi_float
{

constexpr test::sin<float> expected;

void degree_6()
{
    constexpr emstd::close_to<float> equals(1e-1);

    table::sin<float, 6> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_8()
{
    constexpr emstd::close_to<float> equals(1e-3);

    table::sin<float, 8> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_12()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::sin<float, 12> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
}

void degree_14()
{
    constexpr emstd::close_to<float> equals;

    table::sin<float, 14> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
}

void run()
{
    degree_6();
    degree_8();
    degree_12();
    degree_14();
}

} // namespace suite_sin_pi_float

//-----------------------------------------------------------------------------

int main()
{
    suite_sin_pi_float::run();
    return 0;
}
