///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/table/bessel.hpp>
#include <emstd/math/complex.hpp>
#include <emstd/math/dual.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace test
{

template <typename T>
struct bessel_at_1 : emstd::array<T, 24>
{
private:
    using base = emstd::array<T, 24>;

public:
    // BesselJ[n, 1] calculated by Mathematica

    constexpr bessel_at_1()
        : base{ T(7.65197686557966551449717526102663220909274289755325241861547549119278912215272440e-1),
                T(4.40050585744933515959682203718914913127372301992765251136758171780138222478015547e-1),
                T(1.14903484931900480469646881335166605345470314230205260411968794440997532740758655e-1),
                T(1.9563353982668405918905321621751508254508954928055790511117005983851908485019074e-2),
                T(2.476638964109955043785048395342444181583415338129482654733241462113918169355793e-3),
                T(2.49757730211234431375065540988045198158367776980070726748925713059436869827272e-4),
                T(2.0938338002389269965607014538007800000262431671224612756015668480450528916929e-5),
                T(1.502325817436808212218633468048401844781403074624626323262308705969477175876e-6),
                T(9.422344172604500545385401466982582667721137352015576965665340312215154534516e-8),
                T(5.249250179911875043030766668811382053978901697865991244145743984947549645865e-9),
                T(2.63061512368745320699785368779050294408857041432072737969988606904348280411e-10),
                T(1.19800674630313709649407067696238341982391307754635152540281531394159623588e-11),
                T(4.99971817944840528910180152674057952403835628124597618630762162802891482779e-13),
                T(1.92561676448017289036168945535566594529242995268275931101387678534332278922e-14),
                T(6.88540820004422583859105718415193372196159572919802232845801386372442419853e-16),
                T(2.29753153221034444380655620687549685681685149268694095436709649951598636101e-17),
                T(7.18639658680749282861143647455684848895874886280053464327563482353488449813e-19),
                T(2.11537556805326134910346498269465964994814340923013148110664401517667839368e-20),
                T(5.88034457359575834034446660499432086493872858191239248695482806582204040335e-22),
                T(1.54847844121165342054299510329586142979888025832981419709408851925615151810e-23),
                T(3.87350300852465771891478752995256829701639974090146200270830735133536543796e-25),
                T(9.22762198209667022919908685165889007679638030770603989234421277994657081263e-27),
                T(2.09822395594377734882894774416553523807998833507475207626201624219430334749e-28),
                T(4.56342405595010564828322266946497075556836662286924320865868570836391633192e-30) }
    {
    }
};

template <typename T>
struct bessel_at_pi : emstd::array<T, 24>
{
private:
    using base = emstd::array<T, 24>;

public:
    // BesselJ[n, pi] calculated by Mathematica

    constexpr bessel_at_pi()
        : base{ T(-0.3042421776440938642020349128177049239696505347838910032382123499581094663214754),
                T(0.284615343179752757345310599686131405709811181849465727564553646963867567128054101),
                T(0.485433932631509109705495716183189237851952580698689431791082274319072752768953695),
                T(0.333458336202989535390218581739886748302244004453913218354781570550951632685630731),
                T(0.151424577631349710753709559315483807038759925168402337483048179162518460777327008),
                T(0.052141184367118474740460126093102196955821723437313133385905977554891783829069346),
                T(0.014545966982505560573660369604001803894243209439033798760492449679473312151049693),
                T(0.003420316768495789504925687251642531955341854051173950480063776195555323271515558),
                T(0.000696121995588115929992311104476137269705667656783857742457399102385228539103184),
                T(0.000125003442475193113826107929503774086502320588105831554487023901548901375349931),
                T(0.000020094972255377471494082631181844189394747255773227174919956269233923182001349),
                T(2.925124154319571845858558096985975327551927019833358078393480829568822466058e-6),
                T(3.891383505907653044286528948473536999145299334353281129069674634138527889610e-7),
                T(4.767386375149700570022033875150329231323257371531317478584120797118805899235e-8),
                T(5.413265170929142405467107463393478877947965322546956240842764714239782870189e-9),
                T(5.72819220854731575673782309073342965171837651102264567532195194888490498544e-10),
                T(5.67554588955753850515097009761512417018411883302763404921356133159321161283e-11),
                T(5.28713630876956429434949108236398717542587535585005755161223691797604463326e-12),
                T(4.64764831633937827644270504226310789333047379344855011093922003348649184341e-13),
                T(3.86763549770215354707832314313443121886122534846503326777162764261834309984e-14),
                T(3.05568209416738386190311453834232098256607456391147434779745654572979465207e-15),
                T(2.29797807309147972959784582171510211929283552995237422843075712327175499807e-16),
                T(1.64882892067172949319121777705369073000960491228489255775663067152153823744e-17),
                T(1.13115296409870555215633021809121315400367567817788888601218977647471285711e-18) }
    {
    }
};

template <typename T>
struct complex_bessel_at_1_1i : emstd::array<T, 12>
{
private:
    using base = emstd::array<T, 12>;

public:
    // BesselJ[n, 1 + 1 i] calculated by Mathematica

    constexpr complex_bessel_at_1_1i()
        : base{ T(0.9376084768060292765997381974258187169462808245971615634970801987589415823619956,
                  -0.4965299476091221321664597212252381995017918165852524521031038236623926969950530),
                T(0.6141603349229036101692196583755023458870974786980673050283075727514458108287962,
                  0.3650280288270877885133519016286864310032474784307469001289757789224401165995348),
                T(0.0415798869439621220828333625783700599440641325316526416602031529149443450663354,
                  0.247397641513306310510591964478422284617941816317932047203772029833387002765791),
                T(-0.036205278008366744982369004261917656763085580998897927300357207254783115164541,
                  0.046607480311600588342165302171418018344507889141811910958161974914445198799377),
                T(-0.01037328003426059200344446884986897519979720810291069068678884993595809416182,
                  0.001040633446595689463010954821584740704838594104197467571785516674297939125966),
                T(-0.00112530834229286517936505185121928121674887499595496515965612579185750497890,
                  -0.000951826388175462476343607485603154725964680313379277923864508473421065648194),
                T(-0.000012393618081046275098827834243204513770568443760524730814321390434758973681,
                  -0.00017322367600867594790373299350410825091762069131903139282743008211574247239),
                T(0.0000116045777545318413496868847354046286197401854776284178056169565544963024282,
                  -0.0000131539593903155604858234699622676969176331719717620482141436766648353441031),
                T(1.5479466305602411458717376551630356853175383015893179546343496623856819572e-6,
                  -8.6084005255864944839489379598027843992810826701869310894350419579053321910e-8),
                T(9.0323247903168258571101469115434110857634321471171344303037387956726654807e-8,
                  8.1714303786711760133653684179188683150378945432550089914076009117461869568e-8),
                T(3.913346486790224710587244885694607545811005441749533196709112820147620948e-10,
                  8.603508207756458902459315171818994627512442354278021393698010025670254756e-9),
                T(-3.74819338813444835921072511549557036698892486641597169348174879876486292e-10,
                  4.07431804062604180352222653306655578934472668480590826194978319093057049e-10) }
    {
    }
};

template <typename T>
struct dual_bessel_at_1_1e : emstd::array<T, 14>
{
private:
    using base = emstd::array<T, 14>;

public:
    // BesselJ[n, 1 + 1e]

    constexpr dual_bessel_at_1_1e()
        : base{ T(7.65197686557966551449717526102663220909274289755325241861547549119278912215272440e-1,
                  -0.440050585736206984714),
                T(4.40050585744933515959682203718914913127372301992765251136758171780138222478015547e-1,
                  0.325147100818630475298),
                T(1.14903484931900480469646881335166605345470314230205260411968794440997532740758655e-1,
                  0.210243615882719635693),
                T(1.9563353982668405918905321621751508254508954928055790511117005983851908485019074e-2,
                  0.056213422984185922115),
                T(2.476638964109955043785048395342444181583415338129482654733241462113918169355793e-3,
                  0.00965679812626792579498),
                T(2.49757730211234431375065540988045198158367776980070726748925713059436869827272e-4,
                  0.00122785031305800449407),
                T(2.0938338002389269965607014538007800000262431671224612756015668480450528916929e-5,
                  0.000124127702197273947013),
                T(1.502325817436808212218633468048401844781403074624626323262308705969477175876e-6,
                  1.04220572803600376303e-05),
                T(9.422344172604500545385401466982582667721137352015576965665340312215154534516e-8,
                  7.48538283630210856735e-07),
                T(5.249250179911875043030766668811382053978901697865991244145743984947549645865e-9,
                  4.69801901052092499813e-08),
                T(2.63061512368745320699785368779050294408857041432072737969988606904348280411e-10,
                  2.6186350268899681832e-09),
                T(1.19800674630313709649407067696238341982391307754635152540281531394159623588e-11,
                  1.3128022193515292487e-10),
                T(4.99971817944840528910180152674057952403835628124597618630762162802891482779e-13,
                  5.9804056239361879833e-12),
                T(1.92561676448017289036168945535566594529242995268275931101387678534332278922e-14,
                  2.49641101248488503677e-13) }
    {
    }
};

template <typename T>
struct modified_bessel_at_1 : emstd::array<T, 24>
{
private:
    using base = emstd::array<T, 24>;

public:
    // BesselI[n, 1] calculated by Mathematica

    constexpr modified_bessel_at_1()
        : base{ T(1.266065877752008335598244625214717537607670311354962206808135331213575016122775470),
                T(5.65159103992485027207696027609863307328899621621092009480294489479255640964371134e-1),
                T(1.35747669767038281182852569994990922949871068112778187847546352255063734194033202e-1),
                T(2.2168424924331902476285747629899615529415349169979258090109080459000704188238325e-2),
                T(2.737120221046866325138084215593229773378973092902639306891869501059509064603250e-3),
                T(2.71463155956971875181073905153777342383564426758143634974124450524631671412319e-4),
                T(2.2488661477147573327345164055456349543328825321202957150624995813192350480052e-5),
                T(1.599218231200995252931936488301147863618522903708149166624500766323465651688e-6),
                T(9.960624033363978629805321924027945266950466928886881788198508466383135641747e-8),
                T(5.518385862758672163084980456676620906448195086248080512739411702163949008614e-9),
                T(2.75294803983687362523571020100276353437157736403368652675674024880274262413e-10),
                T(1.24897830849249126135600546710938377050403581807074592259312045584637603531e-11),
                T(5.19576115339285025249817336211923926269856427804549705187524594071534642991e-13),
                T(1.99563167820720075644386020076634745638039133982663014306143007469289213931e-14),
                T(7.11879005412828574413684012673587610954679449625867991552774651382686769644e-16),
                T(2.37046305128074808554496528030214570728888087419976671366105082136918431082e-17),
                T(7.40090028604148750194428582943898768015187365937977454459404971931476397264e-19),
                T(2.17495974747208492279381488166964964028130319823885939095491118845983957788e-20),
                T(6.03714463639876444531523176217890319544278536765261534735167855130940782672e-22),
                T(1.58767836852972248033144728524448992190046588391786590830690998845276026359e-23),
                T(3.96683598581902005573207824984149222101500876472489578542059518891882505668e-25),
                T(9.43974202114458038615985307893033494462378027907594138671912885230240922367e-27),
                T(2.14433693829629354493995669075154427302104751300040299856107095181318274798e-28),
                T(4.65949264088878842404363962354014333117122187416819305041666432440513252794e-30) }
    {
    }
};

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_bessel_1_float
{

constexpr test::bessel_at_1<float> expected;

void degree_4()
{
    constexpr emstd::close_to<float> equals(1e-2);

    table::bessel<float, 4> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
}

void degree_5()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::bessel<float, 5> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
}

void degree_6()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::bessel<float, 6> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_7()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::bessel<float, 6> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_8()
{
    constexpr emstd::close_to<float> equals;

    table::bessel<float, 8> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_24()
{
    constexpr emstd::close_to<float> equals;

    table::bessel<float, 24> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
    assert(equals(result[14], expected[14]));
    assert(equals(result[15], expected[15]));
    assert(equals(result[16], expected[16]));
    assert(equals(result[17], expected[17]));
    assert(equals(result[18], expected[18]));
    assert(equals(result[19], expected[19]));
    assert(equals(result[20], expected[20]));
    assert(equals(result[21], expected[21]));
    assert(equals(result[22], expected[22]));
    assert(equals(result[23], expected[23]));
}

void run()
{
    degree_4();
    degree_5();
    degree_6();
    degree_7();
    degree_8();
    degree_24();
}

} // namespace suite_bessel_1_float

//-----------------------------------------------------------------------------

namespace suite_bessel_pi_float
{

constexpr test::bessel_at_pi<float> expected;

void degree_8()
{
    constexpr emstd::close_to<float> equals(1e-3);

    table::bessel<float, 8> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_10()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::bessel<float, 10> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
}

void degree_12()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::bessel<float, 12> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
}

void degree_14()
{
    constexpr emstd::close_to<float> equals;

    table::bessel<float, 14> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
}

void degree_24()
{
    constexpr emstd::close_to<float> equals;

    table::bessel<float, 24> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
    assert(equals(result[14], expected[14]));
    assert(equals(result[15], expected[15]));
    assert(equals(result[16], expected[16]));
    assert(equals(result[17], expected[17]));
    assert(equals(result[18], expected[18]));
    assert(equals(result[19], expected[19]));
    assert(equals(result[20], expected[20]));
    assert(equals(result[21], expected[21]));
    assert(equals(result[22], expected[22]));
    assert(equals(result[23], expected[23]));
}

void run()
{
    degree_8();
    degree_10();
    degree_12();
    degree_14();
    degree_24();
}

} // namespace suite_bessel_pi_float

//-----------------------------------------------------------------------------

namespace suite_bessel_complex_float
{

void bessel12_1_0i()
{
    constexpr emstd::close_to<float> equals;
    constexpr test::bessel_at_1<float> expected;

    constexpr table::bessel<complex::number<float>, 12> result({1.0f, 0.0f});
    assert(equals(real(result[0]), expected[0]));
    assert(equals(imag(result[0]), 0.0f));
    assert(equals(real(result[1]), expected[1]));
    assert(equals(imag(result[1]), 0.0f));
    assert(equals(real(result[2]), expected[2]));
    assert(equals(imag(result[2]), 0.0f));
    assert(equals(real(result[3]), expected[3]));
    assert(equals(imag(result[3]), 0.0f));
    assert(equals(real(result[4]), expected[4]));
    assert(equals(imag(result[4]), 0.0f));
    assert(equals(real(result[5]), expected[5]));
    assert(equals(imag(result[5]), 0.0f));
    assert(equals(real(result[6]), expected[6]));
    assert(equals(imag(result[6]), 0.0f));
    assert(equals(real(result[7]), expected[7]));
    assert(equals(imag(result[7]), 0.0f));
    assert(equals(real(result[8]), expected[8]));
    assert(equals(imag(result[8]), 0.0f));
    assert(equals(real(result[9]), expected[9]));
    assert(equals(imag(result[9]), 0.0f));
    assert(equals(real(result[10]), expected[10]));
    assert(equals(imag(result[10]), 0.0f));
    assert(equals(real(result[11]), expected[11]));
    assert(equals(imag(result[11]), 0.0f));
}

void bessel12_1_1i()
{
    constexpr emstd::close_to<float> equals;
    constexpr test::complex_bessel_at_1_1i<complex::number<float>> expected;

    constexpr table::bessel<complex::number<float>, 12> result({1.0f, 1.0f});
    assert(equals(real(result[0]), real(expected[0])));
    assert(equals(imag(result[0]), imag(expected[0])));
    assert(equals(real(result[1]), real(expected[1])));
    assert(equals(imag(result[1]), imag(expected[1])));
    assert(equals(real(result[2]), real(expected[2])));
    assert(equals(imag(result[2]), imag(expected[2])));
    assert(equals(real(result[3]), real(expected[3])));
    assert(equals(imag(result[3]), imag(expected[3])));
    assert(equals(real(result[4]), real(expected[4])));
    assert(equals(imag(result[4]), imag(expected[4])));
    assert(equals(real(result[5]), real(expected[5])));
    assert(equals(imag(result[5]), imag(expected[5])));
    assert(equals(real(result[6]), real(expected[6])));
    assert(equals(imag(result[6]), imag(expected[6])));
    assert(equals(real(result[7]), real(expected[7])));
    assert(equals(imag(result[7]), imag(expected[7])));
    assert(equals(real(result[8]), real(expected[8])));
    assert(equals(imag(result[8]), imag(expected[8])));
    assert(equals(real(result[9]), real(expected[9])));
    assert(equals(imag(result[9]), imag(expected[9])));
    assert(equals(real(result[10]), real(expected[10])));
    assert(equals(imag(result[10]), imag(expected[10])));
    assert(equals(real(result[11]), real(expected[11])));
    assert(equals(imag(result[11]), imag(expected[11])));
}

void run()
{
    bessel12_1_0i();
    bessel12_1_1i();
}

} // namespace suite_bessel_1_0i_complex_float

//-----------------------------------------------------------------------------

namespace suite_bessel_dual_float
{

void bessel12_1_0i()
{
    constexpr emstd::close_to<float> equals;
    constexpr test::bessel_at_1<float> expected;

    constexpr table::bessel<dual::number<float>, 12> result({1.0f, 0.0f});
    assert(equals(real(result[0]), expected[0]));
    assert(equals(imag(result[0]), 0.0f));
    assert(equals(real(result[1]), expected[1]));
    assert(equals(imag(result[1]), 0.0f));
    assert(equals(real(result[2]), expected[2]));
    assert(equals(imag(result[2]), 0.0f));
    assert(equals(real(result[3]), expected[3]));
    assert(equals(imag(result[3]), 0.0f));
    assert(equals(real(result[4]), expected[4]));
    assert(equals(imag(result[4]), 0.0f));
    assert(equals(real(result[5]), expected[5]));
    assert(equals(imag(result[5]), 0.0f));
    assert(equals(real(result[6]), expected[6]));
    assert(equals(imag(result[6]), 0.0f));
    assert(equals(real(result[7]), expected[7]));
    assert(equals(imag(result[7]), 0.0f));
    assert(equals(real(result[8]), expected[8]));
    assert(equals(imag(result[8]), 0.0f));
    assert(equals(real(result[9]), expected[9]));
    assert(equals(imag(result[9]), 0.0f));
    assert(equals(real(result[10]), expected[10]));
    assert(equals(imag(result[10]), 0.0f));
    assert(equals(real(result[11]), expected[11]));
    assert(equals(imag(result[11]), 0.0f));
}

void bessel14_1_1i()
{
    constexpr emstd::close_to<float> equals;
    constexpr test::dual_bessel_at_1_1e<dual::number<float>> expected;

    constexpr table::bessel<dual::number<long double>, 14> result({1.0f, 1.0f});
    assert(equals(real(result[0]), real(expected[0])));
    assert(equals(imag(result[0]), imag(expected[0])));
    assert(equals(real(result[1]), real(expected[1])));
    assert(equals(imag(result[1]), imag(expected[1])));
    assert(equals(real(result[2]), real(expected[2])));
    assert(equals(imag(result[2]), imag(expected[2])));
    assert(equals(real(result[3]), real(expected[3])));
    assert(equals(imag(result[3]), imag(expected[3])));
    assert(equals(real(result[4]), real(expected[4])));
    assert(equals(imag(result[4]), imag(expected[4])));
    assert(equals(real(result[5]), real(expected[5])));
    assert(equals(imag(result[5]), imag(expected[5])));
    assert(equals(real(result[6]), real(expected[6])));
    assert(equals(imag(result[6]), imag(expected[6])));
    assert(equals(real(result[7]), real(expected[7])));
    assert(equals(imag(result[7]), imag(expected[7])));
    assert(equals(real(result[8]), real(expected[8])));
    assert(equals(imag(result[8]), imag(expected[8])));
    assert(equals(real(result[9]), real(expected[9])));
    assert(equals(imag(result[9]), imag(expected[9])));
    assert(equals(real(result[10]), real(expected[10])));
    assert(equals(imag(result[10]), imag(expected[10])));
    assert(equals(real(result[11]), real(expected[11])));
    assert(equals(imag(result[11]), imag(expected[11])));
}

void run()
{
    bessel12_1_0i();
    bessel14_1_1i();
}

} // namespace suite_bessel_dual_float

//-----------------------------------------------------------------------------

namespace suite_modified_bessel_1_float
{

constexpr test::modified_bessel_at_1<float> expected;

void degree_4()
{
    constexpr emstd::close_to<float> equals(1e-2);

    table::modified_bessel<float, 4> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
}

void degree_5()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::modified_bessel<float, 5> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
}

void degree_6()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::modified_bessel<float, 6> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_7()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::modified_bessel<float, 7> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
}

void degree_8()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::modified_bessel<float, 8> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_9()
{
    constexpr emstd::close_to<float> equals;

    table::modified_bessel<float, 9> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
}

void degree_24()
{
    constexpr emstd::close_to<float> equals;

    table::modified_bessel<float, 24> result(1.0f);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
    assert(equals(result[14], expected[14]));
    assert(equals(result[15], expected[15]));
    assert(equals(result[16], expected[16]));
    assert(equals(result[17], expected[17]));
    assert(equals(result[18], expected[18]));
    assert(equals(result[19], expected[19]));
    assert(equals(result[20], expected[20]));
    assert(equals(result[21], expected[21]));
    assert(equals(result[22], expected[22]));
    assert(equals(result[23], expected[23]));
}

void run()
{
    degree_4();
    degree_5();
    degree_6();
    degree_7();
    degree_8();
    degree_9();
    degree_24();
}

} // namespace suite_modified_bessel_1_float

//-----------------------------------------------------------------------------

int main()
{
    suite_bessel_1_float::run();
    suite_bessel_pi_float::run();
    suite_bessel_complex_float::run();
    suite_bessel_dual_float::run();
    suite_modified_bessel_1_float::run();
    return 0;
}
