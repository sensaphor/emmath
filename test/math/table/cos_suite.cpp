///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/numbers.hpp>
#include <emstd/math/table/cos.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace test
{

template <typename T>
struct cos : emstd::array<T, 24>
{
private:
    using base = emstd::array<T, 24>;

public:
    // (-1)^n BesselJ[2n, pi] calculated by Mathmatica

    constexpr cos()
        : base{ T(-0.3042421776440938642020349128177049239696505347838910032382123499581094663214754),
                T(0),
                T(-0.485433932631509109705495716183189237851952580698689431791082274319072752768953695),
                T(0),
                T(0.151424577631349710753709559315483807038759925168402337483048179162518460777327008),
                T(0),
                T(-0.014545966982505560573660369604001803894243209439033798760492449679473312151049693),
                T(0),
                T(0.000696121995588115929992311104476137269705667656783857742457399102385228539103184),
                T(0),
                T(-0.000020094972255377471494082631181844189394747255773227174919956269233923182001349),
                T(0),
                T(3.891383505907653044286528948473536999145299334353281129069674634138527889610e-7),
                T(0),
                T(-5.413265170929142405467107463393478877947965322546956240842764714239782870189e-9),
                T(0),
                T(5.67554588955753850515097009761512417018411883302763404921356133159321161283e-11),
                T(0),
                T(-4.64764831633937827644270504226310789333047379344855011093922003348649184341e-13),
                T(0),
                T(3.05568209416738386190311453834232098256607456391147434779745654572979465207e-15),
                T(0),
                T(-1.64882892067172949319121777705369073000960491228489255775663067152153823744e-17),
                T(0) }
    {
    }
};

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_cos_pi_float
{

constexpr test::cos<float> expected;

void degree_6()
{
    constexpr emstd::close_to<float> equals(1e-1);

    table::cos<float, 6> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_7()
{
    constexpr emstd::close_to<float> equals(1e-2);

    table::cos<float, 7> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
}

void degree_8()
{
    constexpr emstd::close_to<float> equals(1e-2);

    table::cos<float, 8> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_12()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::cos<float, 12> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
}

void degree_14()
{
    constexpr emstd::close_to<float> equals;

    table::cos<float, 14> result(numbers::pi_v<float>);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
    assert(equals(result[12], expected[12]));
    assert(equals(result[13], expected[13]));
}

void run()
{
    degree_6();
    degree_7();
    degree_8();
    degree_12();
    degree_14();
}

} // namespace suite_cos_pi_float

//-----------------------------------------------------------------------------

int main()
{
    suite_cos_pi_float::run();
    return 0;
}
