///////////////////////////////////////////////////////////////////////////////
//
// Copyright (C) 2023 Bjorn Reese <breese@users.sourceforge.net>
//
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)
//
///////////////////////////////////////////////////////////////////////////////

#include <emstd/cassert.hpp>
#include <emstd/functional.hpp>
#include <emstd/math/table/exp.hpp>

using namespace emstd::math;

//-----------------------------------------------------------------------------

namespace test
{

template <typename T>
struct exp : emstd::array<T, 12>
{
private:
    using base = emstd::array<T, 12>;

public:
    // (BesselI[n, 1] calculated by Mathmatica

    constexpr exp()
        : base{ T(1.266065877752008335598244625214717537607670311354962206808135331213575016122775470),
                T(0.565159103992485027207696027609863307328899621621092009480294489479255640964371134),
                T(0.135747669767038281182852569994990922949871068112778187847546352255063734194033202),
                T(0.022168424924331902476285747629899615529415349169979258090109080459000704188238325),
                T(0.002737120221046866325138084215593229773378973092902639306891869501059509064603250),
                T(0.000271463155956971875181073905153777342383564426758143634974124450524631671412319),
                T(0.000022488661477147573327345164055456349543328825321202957150624995813192350480052),
                T(1.599218231200995252931936488301147863618522903708149166624500766323465651688e-6),
                T(9.960624033363978629805321924027945266950466928886881788198508466383135641747e-8),
                T(5.518385862758672163084980456676620906448195086248080512739411702163949008614e-9),
                T(2.75294803983687362523571020100276353437157736403368652675674024880274262413e-10),
                T(1.24897830849249126135600546710938377050403581807074592259312045584637603531e-11) }
    {
    }
};

} // namespace test

//-----------------------------------------------------------------------------

namespace suite_exp_float
{

constexpr test::exp<float> expected;

void degree_4()
{
    constexpr emstd::close_to<float> equals(1e-2);

    table::exp<float, 4> result(1);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
}

void degree_6()
{
    constexpr emstd::close_to<float> equals(1e-4);

    table::exp<float, 6> result(1);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
}

void degree_8()
{
    constexpr emstd::close_to<float> equals(1e-6);

    table::exp<float, 8> result(1);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
}

void degree_10()
{
    constexpr emstd::close_to<float> equals;

    table::exp<float, 10> result(1);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
}

void degree_12()
{
    constexpr emstd::close_to<float> equals;

    table::exp<float, 12> result(1);
    assert(equals(result[0], expected[0]));
    assert(equals(result[1], expected[1]));
    assert(equals(result[2], expected[2]));
    assert(equals(result[3], expected[3]));
    assert(equals(result[4], expected[4]));
    assert(equals(result[5], expected[5]));
    assert(equals(result[6], expected[6]));
    assert(equals(result[7], expected[7]));
    assert(equals(result[8], expected[8]));
    assert(equals(result[9], expected[9]));
    assert(equals(result[10], expected[10]));
    assert(equals(result[11], expected[11]));
}

void run()
{
    degree_4();
    degree_6();
    degree_8();
    degree_10();
    degree_12();
}

} // namespace suite_exp_float

//-----------------------------------------------------------------------------

int main()
{
    suite_exp_float::run();
    return 0;
}
